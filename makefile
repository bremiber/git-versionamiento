#makefile para encontrar la distancia entre dos puntos en el espacio.
IDIR=./includes/
CC=gcc
CFLAGS=-c -lm -I.

all: main

main: main.o point.o
	$(CC) -o main main.o point.o -lm -I$(IDIR)

main.o: main.c $(IDIR)point.h
	$(CC) $(CFLAGS) main.c

point.o: point.c $(IDIR)point.h
	$(CC) $(CFLAGS) point.c

clean:
	rm *o main
