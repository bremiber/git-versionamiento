#include <stdio.h>
#include <string.h>
#include <math.h>
#include "includes/point.h"


float distanciaEuclidiana(float x1, float y1, float z1, float x2, float y2, float z2){
	float distancia;
	distancia=sqrt(((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1))+((z2-z1)*(z2-z1)));
	return distancia;
}

point punto_medio(point a, point b){
	point resultado;
	resultado.x=(a.x+b.x)/2;
	resultado.y=(a.y+b.y)/2;
	resultado.z=(a.z+b.z)/2;
	return resultado;
}
