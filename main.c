#include <stdio.h>
#include <string.h>
#include <math.h>
#include "includes/point.h"

struct point p1;
struct point p2;
struct point puntoMedio;

int main()
{
    int i;
    float distancia;

    printf("DISTANCIA ENTRE DOS PUNTOS\n");

	printf("Ingrese las coordenadas del punto Inicial: \n");
	printf("x1 ->  ");
	scanf("%f",&p1.x);
	printf("y1 ->  ");
	scanf("%f",&p1.y);
	printf("z1 ->  ");
	scanf("%f",&p1.z);

	printf("Ingrese las coordenadas del punto Final:\n");
	printf("x2 ->  ");
	scanf("%f",&p2.x);
	printf("y2 ->  ");
	scanf("%f",&p2.y);
	printf("z2 ->  ");
	scanf("%f",&p2.z);

	distancia=distanciaEuclidiana(p1.x, p1.y, p1.z, p2.x, p2.y, p2.z);
	printf("La distancia euclidiana es: %.2f \n",distancia);

	puntoMedio=punto_medio(p1, p2);
	printf("\nPUNTO MEDIO ENTRE LOS DOS PUNTOS\n");
	printf("Las coordenadas del punto medio son: x=%.2f, y=%.2f, z=%.2f\n",puntoMedio.x, puntoMedio.y, puntoMedio.z);
	return 0;
}
